import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String address;
        int port;
        Socket socket;
        DataInputStream in;
        String message;

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter server ip or domain name: ");
        address = scanner.nextLine();

        System.out.print("Enter server's port: ");
        port = scanner.nextInt();

        try {
            //connect to server.
            socket = new Socket(address, port);
            System.out.println("Connection is successfull.");

            //listen server
            in = new DataInputStream(socket.getInputStream());
            
            while(true) {
                message = in.readUTF();
                System.out.println("incoming message: " + message);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}